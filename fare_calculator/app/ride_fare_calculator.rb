# frozen_string_literal: true

require_relative './requires.rb'

class RideFareCalculator
  def call(env)
    request = Rack::Request.new(env)
    params = prepare_params(request)

    # TODO: required params are duration and distance. return BadRequest if not present.
    calc_result = RideFareCalculator::Calculator.call(duration: {
                                                        value: params[:'duration[value]'],
                                                        units: params[:'duration[units]']
                                                      },
                                                      distance: {
                                                        value: params[:'distance[value]'],
                                                        units: params[:'distance[units]']
                                                      })

    # TODO: Concrete errors and status codes
    if calc_result.first == :ok
      [200, { 'Content-Type' => 'application/json' }, [{ ride_fare: calc_result.last }.to_json]]
    else
      [500, { 'Content-Type' => 'application/json' }, [{ error: 'generic error' }.to_json]]
    end
  end

  private

  def prepare_params(req)
    CGI.parse(req.query_string).deep_symbolize_keys.each_with_object({}) do |(k, v), a|
      a[k] = process_numeric_types(v.last)
    end
  end

  def process_numeric_types(value)
    float = value.to_f
    int = value.to_i
    if int.to_s == value
      int
    elsif float.to_s == value
      float
    else
      value
    end
  end
end
