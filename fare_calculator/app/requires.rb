# frozen_string_literal: true

# TODO: https://bundler.io/v1.3/bundler_setup.html

# require 'pry'
# TODO: сделать условную подгрузку гемов в зависимости от окружения

require 'json'
require 'cgi'
require 'active_support'
require 'active_support/core_ext/hash/keys'

require './app/ride_fare_calculator/tariff_storage'
require './app/ride_fare_calculator/calculator'
