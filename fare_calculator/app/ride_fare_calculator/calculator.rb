# frozen_string_literal: true

class RideFareCalculator
  class Calculator
    TS = TariffStorage

    class << self
      def call(duration:, distance:)
        sum = TS.fixed_fee + duration_fee(duration) + distance_fee(distance)
        [:ok, { value: [sum, TS.min_price].max, units: TS.currency_units }]
      end

      private

      # duration { value: , units: }
      # округляем до ближайшей полной минуты
      # сейчас это умеет работать только с секундами, подающимися на вход
      # TODO: описать процесс округления (метры - до км с одной цифрой после запятой, деньги - до рубля)
      # TODO: или вместо экспешена - сделать в начале валидацию параметров.
      def duration_fee(duration)
        raise StandardError unless duration[:units] == 'second'
        value = rounded { duration[:value].to_f / 60 }
        rounded do
          value * TS.per_duration[:value]
        end
      end

      # distance { value: , units: }
      # округляем значение в км до десятых
      # сейчас это умеет работать только с метрами
      # TODO: описать процесс округления (метры - до км с одной цифрой после запятой, деньги - до рубля)
      # TODO: или вместо экспешена - сделать в начале валидацию параметров.
      def distance_fee(distance)
        raise StandardError unless distance[:units] == 'meter'
        value = rounded(1) { distance[:value].to_f / 1000 }
        rounded do
          value * TS.per_distance[:value]
        end
      end

      def rounded(num = 0)
        yield.round(num)
      end
    end
  end
end
