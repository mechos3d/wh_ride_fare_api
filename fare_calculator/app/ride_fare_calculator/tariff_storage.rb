# frozen_string_literal: true

# TODO: add frozen_string_literal everywhere in the app !

class RideFareCalculator
  module TariffStorage
    CURRENCY_UNITS = 'ruble'
    MIN_PRICE = 500
    FIXED_FEE = 250
    PER_DISTANCE = { value: 20, unit: 'km' }.freeze
    PER_DURATION = { value: 20, unit: 'minute' }.freeze

    class << self
      def currency_units
        CURRENCY_UNITS
      end

      def min_price
        MIN_PRICE
      end

      def fixed_fee
        FIXED_FEE
      end

      def per_distance
        PER_DISTANCE
      end

      def per_duration
        PER_DURATION
      end
    end
  end
end
