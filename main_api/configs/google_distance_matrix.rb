GoogleDistanceMatrix.configure_defaults do |config|
  config.mode = 'driving'
  config.units = 'metric'

  # If you have an API key, you can specify that as well.
  config.google_api_key = ENV['WH_APP_GOOGLE_API_KEY']
end
