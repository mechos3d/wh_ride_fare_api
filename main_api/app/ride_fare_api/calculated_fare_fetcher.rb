module RideFareApi
  class CalculatedFareFetcher
    def call(metrics:)
      conn = Faraday.new(url: ::RideFareApi::Settings::FARE_CALCULATOR_URL)

      res = conn.get do |req|
        req.params['duration'] = metrics[:duration]
        req.params['distance'] = metrics[:distance]
      end
      res.body # TODO: implement non-success cases
    end
  end
end
