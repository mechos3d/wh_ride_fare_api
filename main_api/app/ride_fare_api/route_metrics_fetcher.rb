module RideFareApi
  class RouteMetricsFetcher
    class << self
      def call(origin:, destination:)
        ::RideFareApi::GoogleDistanceMatrix.new.make_request(origin: origin, destination: destination)
      end
    end
  end
end
