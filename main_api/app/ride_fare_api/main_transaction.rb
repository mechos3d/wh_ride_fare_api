module RideFareApi
  class MainTransaction
    include Dry::Transaction

    step :validate_input
    step :fetch_ride_metrics
    step :validate_ride_metrics
    step :fetch_calculated_fare

    private

    def validate_input(input)
      Success(input) # TODO: implement validation
    end

    def fetch_ride_metrics(input)
      metrics = RouteMetricsFetcher.call(origin:      input[:origin],
                                         destination: input[:destination])
      Success(input.merge(metrics: metrics)) # TODO: implement failure case
    end

    def validate_ride_metrics(input)
      Success(input) # TODO: implement validation
    end

    def fetch_calculated_fare(input)
      fare_data = CalculatedFareFetcher.new.call(metrics: input[:metrics])
      Success(fare_data) # TODO: implement failure case
    end
  end
end
