module RideFareApi
  class GoogleDistanceMatrix
    def make_request(origin:, destination:)
      matrix = ::GoogleDistanceMatrix::Matrix.new

      # TODO: упростить до строк через запятую, как в гугл апи, не связываться с хэшами
      origin_place = ::GoogleDistanceMatrix::Place.new(lng: origin[:lng], lat: origin[:lat])
      dest_place = ::GoogleDistanceMatrix::Place.new(lng: destination[:lng], lat: destination[:lat])

      matrix.origins << origin_place
      matrix.destinations << dest_place

      route = matrix.shortest_route_by_distance_to(dest_place)
      # NOTE: TODO: route может быть nil'ом
      {
        distance: {
          value: route.distance_in_meters,
          units: 'meter'
        },
        duration: {
          value: route.duration_in_seconds,
          units: 'second'

        }
      }
    end
  end
end

# Пример ответа:
# {
# "destination_addresses" : [ "Skolkovskoye Shosse, 45, Scolkovo, Moscow Oblast, Russia, 143026" ],
# "origin_addresses" : [
# "Ulitsa 4-Ya Zaprudnaya, 10, Nemchinovka, Moskovskaya oblast', Russia, 143025"
# ],
# "rows" : [
# {
# "elements" : [
# {
# "distance" : {
# "text" : "5.1 km",
# "value" : 5097
# },
# "duration" : {
# "text" : "9 mins",
# "value" : 565
# },
# "status" : "OK"
# }
# ]
# }
# ],
# "status" : "OK"
# }
