# TODO: https://bundler.io/v1.3/bundler_setup.html

# TODO: сделать условную подгрузку не-production гемов
# require 'pry'

require 'rubygems'
require 'sinatra/base'
require 'google_distance_matrix'
require 'json'
require 'net/http'
require 'active_support'
require 'active_support/core_ext/hash/keys'
require 'dry/transaction'
require 'faraday'

require_relative '../configs/google_distance_matrix.rb'
require_relative '../configs/settings.rb'

require_relative './ride_fare_api/calculated_fare_fetcher.rb'
require_relative './ride_fare_api/google_distance_matrix.rb'
require_relative './ride_fare_api/main_transaction.rb'
require_relative './ride_fare_api/route_metrics_fetcher.rb'
