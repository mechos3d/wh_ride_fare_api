require_relative 'requires.rb'

module RideFareApi
  class Controller < Sinatra::Base
    before do
      content_type 'application/json'
    end

    # NOTE: example request:
    # /api/v1/ride_fare?origin[lat]=55.9695062&origin[lng]=37.4108329
    # &destination[lat]=55.7826098&destination[lng]=37.5609563"
    get '/api/v1/ride_fare' do
      result = RideFareApi::MainTransaction.new.call(prepared_params(params))
      if result.success?
        { result: result.success }.to_json
      else # TODO: implement different cases - for 500/400, with returning specific error messages
        400
      end
    end

    private

    def prepared_params(params)
      white_listed = params.select do |k, _v|
        %w[origin destination].include?(k)
      end
      white_listed.deep_symbolize_keys
    end
  end
end
