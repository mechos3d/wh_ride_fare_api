# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.
FROM phusion/baseimage:0.10.1

## Create a user for the web app.
RUN addgroup --gid 9999 app \
  && adduser --uid 9999 --gid 9999 --disabled-password --gecos "Application" app \
  && usermod -L app \
  && mkdir -p /home/app/.ssh \
  && chmod 700 /home/app/.ssh \
  && chown app:app /home/app/.ssh

## Install ruby 2.4.4, rubygems and bundler
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends wget \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends make \
    && wget -O ruby-install-0.6.1.tar.gz https://github.com/postmodern/ruby-install/archive/v0.6.1.tar.gz \
    && tar -xzvf ruby-install-0.6.1.tar.gz \
    && cd ruby-install-0.6.1/ \
    && make install \
    && ruby-install --system ruby 2.4.4

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends rubygems

RUN gem install bundler --no-document

RUN mkdir -p /home/app/webapp
COPY Gemfile Gemfile.lock /home/app/webapp/

RUN cd /home/app/webapp && bundle install --without test development --jobs 4 --retry 5 --deployment --path /webapp/vendor/bundle

WORKDIR /home/app/webapp
ADD ./ /home/app/webapp

RUN chown -R app:app /home/app
USER app

CMD ["bundle", "exec", "puma", "-p", "3000" ,"config.ru"]
